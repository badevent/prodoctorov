<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    use HasFactory;

    public function getProfiles()
    {
        return $this->hasManyThrough(Profiles::class, DoctorsProfiles::class, 'doctor_id', 'id', 'id', 'profile_id')->get();
    }
}
