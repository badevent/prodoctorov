<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    use HasFactory;

    public function getService()
    {
        return $this->hasOne(Services::class, 'service_id', 'id');
    }

    public function getDoctors()
    {
        return $this->hasManyThrough(Doctors::class, DoctorsProfiles::class, 'doctor_id', 'id', 'id', 'profile_id')->get();
    }
}
