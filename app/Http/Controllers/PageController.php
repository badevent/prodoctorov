<?php

namespace App\Http\Controllers;

use App\Models\Cities;
use App\Models\Clinics;

class PageController extends Controller
{
    public function index(Cities $cities, Clinics $clinics)
    {
        return view('welcome', ['cities' => $cities->get(), 'clinics' => $clinics->get()]);
    }
}
