<?php

namespace App\Http\Controllers;

use App\Models\Clinics;
use App\Models\ClinicsDoctors;
use App\Models\Doctors;
use App\Models\DoctorsProfiles;
use App\Models\Profiles;
use App\Models\Services;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request, Services $servicesModel, Profiles $profilesModel, Doctors $doctorsModel, DoctorsProfiles $doctorsProfilesModel, Clinics $clinicsModel, ClinicsDoctors $clinicsDoctorsModel)
    {
        $services = $servicesModel->where('name', 'like', '%' . $request->input('query') . '%')->get();

        $profilesObj = [];
        foreach ($services as $service) {
            if ($service->getProfiles()->count()) {
                $profilesObj[] = $service->getProfiles();
            }
        }

        $doctorsObj = [];
        foreach ($profilesObj as $item) {
            foreach ($item as &$profile) {
                $doctorsObj[] = $profile->getDoctors();
            }
        }
    }
}
