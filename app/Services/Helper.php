<?php

namespace App\Services;

class Helper
{
    /**
     * @param $items
     * @param $keys
     * @return array
     */
    public function combineDataKeys($items, $keys): array
    {
        $arr = [];

        foreach ($items as $item) {
            $arr[] = array_fill_keys($keys, $item);
        }

        return $arr;
    }
}
