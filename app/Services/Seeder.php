<?php

namespace App\Services;

/**
 * @param DataService $dataService
 * @param Helper $helper
 */
class Seeder extends \Illuminate\Database\Seeder
{
    public DataService $dataService;
    public Helper $helper;

    public function __construct()
    {
        $this->dataService = new DataService();
        $this->helper = new Helper();
    }
}
