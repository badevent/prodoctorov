<?php

namespace Database\Seeders;

use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class ClinicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clinics')->insert([
            [
                'name' => 'Медицинский центр «Генелли» на Шевченко',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «Мульти Клиник» на Белинского',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «ПрофМедЦентр»',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => '«Доктор Рядом»',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Стоматология «Смайл клиник» на Ленина',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «Сибирский Доктор»',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «Медика»',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «Мульти Клиник» на Сибирской',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => 'Медицинский центр «Ева и Адам»',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
            [
                'name' => '«Медкабинет 70» на Ленина',
                'address' => 'ул. Шевченко, д. 19',
                'phone' => '79610001122',
                'city_id' => random_int(1, 1126)
            ],
        ]);
    }
}
