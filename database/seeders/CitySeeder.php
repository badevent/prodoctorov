<?php

namespace Database\Seeders;

use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('cities')->insert($this->helper->combineDataKeys($this->dataService->getCities(), ['name']));
    }
}
