<?php

namespace Database\Seeders;

use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = $this->helper->combineDataKeys($this->dataService->getProfiles(), ['name', 'service_id']);

        foreach ($arr as $key => $item) {
            $arr[$key] = array_replace($item, ['service_id' => 5]);
        }

        DB::table('profiles')->insert($arr);
    }
}
