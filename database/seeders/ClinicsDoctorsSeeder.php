<?php

namespace Database\Seeders;

use App\Models\Doctors;
use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class ClinicsDoctorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [];

        foreach (Doctors::all() as $item) {
            $arr[] = [
                'clinic_id' => random_int(1, 10),
                'doctor_id' => $item->id,
            ];
        }

        DB::table('clinics_doctors')->insert($arr);
    }
}
