<?php

namespace Database\Seeders;

use App\Models\Doctors;
use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorsProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [];

        foreach (Doctors::all() as $item) {
            $arr[] = [
                'profile_id' => random_int(1, 10),
                'doctor_id' => $item->id,
                'price'=>random_int(500, 30000)
            ];
        }

        DB::table('doctors_profiles')->insert($arr);
    }
}
