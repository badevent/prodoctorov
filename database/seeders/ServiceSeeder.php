<?php

namespace Database\Seeders;

use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert($this->helper->combineDataKeys($this->dataService->getServices(), ['name']));
    }
}
