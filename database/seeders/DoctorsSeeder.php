<?php

namespace Database\Seeders;

use App\Services\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Exception
     */
    public function run()
    {
        $arr = [];

        $i = 0;
        while ($i < 100) {
            $arr[] = [
                'first_name' => $this->dataService->getFirstNames()[random_int(0, 185)],
                'second_name' => $this->dataService->getSecondNames()[random_int(0, 64)],
                'third_name' => $this->dataService->getThirdNames()[random_int(0, 49)],
            ];

            $i++;
        }

        DB::table('doctors')->insert($arr);
    }
}
